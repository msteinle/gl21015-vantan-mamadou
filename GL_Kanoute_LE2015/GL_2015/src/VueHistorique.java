import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.ScrollPane;
import java.awt.TextArea;
import java.io.IOException;

import javax.swing.JSpinner;

import java.awt.Scrollbar;

// La vue Historique sert � afficher les informations concernant le service accompli par un moniteur X
public class VueHistorique extends JFrame {

	public JPanel contentPane;
	public JLabel labelMenuGeneral;
	public JLabel labelInfo;
	public JLabel labelNom;
	public JLabel labelPrenom;
	public String nom;
	public String prenom;
	public VueLogin vuelogin;
	public VueMenuGeneral vueMenugeneral;
	public ModelHistorique modelHistorique;
	public TextArea textArea;
	
	private JButton btnNewButton_1;
	
	
	public VueHistorique() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 539, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 11, 531, 342);
		contentPane.add(panel);
		panel.setLayout(null);
		
		labelMenuGeneral = new JLabel("Historique");
		labelMenuGeneral.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelMenuGeneral.setHorizontalAlignment(SwingConstants.CENTER);
		labelMenuGeneral.setBounds(182, 62, 140, 35);
		panel.add(labelMenuGeneral);
		
		labelInfo = new JLabel("Connect\u00E9 en tant que :");
		labelInfo.setBounds(55, 11, 130, 25);
		panel.add(labelInfo);
		
		labelNom = new JLabel("");
		labelNom.setBounds(195, 11, 110, 25);
		panel.add(labelNom);
		
		labelPrenom = new JLabel("");
		labelPrenom.setBounds(315, 11, 121, 25);
		panel.add(labelPrenom);
		
		JButton btnNewButton = new JButton("Retour Menu");
		btnNewButton.setBackground(new Color(60, 179, 113));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				setVisible(false);
				vueMenugeneral.setVisible(true);
				
			}
		});
		btnNewButton.setBounds(399, 87, 110, 44);
		panel.add(btnNewButton);
		
		textArea = new TextArea();
		textArea.setBounds(10, 160, 499, 172);
		panel.add(textArea);
		
		btnNewButton_1 = new JButton("Actualiser");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				modelHistorique.setNom(nom);
				modelHistorique.setPrenom(prenom);
				textArea.setText("");
				String str;
				for (int i=0;i<300-1;i++){
					
					str = modelHistorique.fonctionHistorique(i);
					if (str!="espace"){

						textArea.setText(str+" \n"+textArea.getText());
						
					}
				}
				
				
				
			}
		});
		btnNewButton_1.setBounds(10, 119, 110, 35);
		panel.add(btnNewButton_1);
		
		
	}
	
	//Mutateur vueLogin
	public void setVueLogin(VueLogin pvuelogin){
		vuelogin=pvuelogin;
		
	}
	
	
	
	public void setNomPrenom(String pNom, String pPrenom){
		nom = pNom;
		prenom = pPrenom;
		
	}
	
	public void setVueMenuGeneral(VueMenuGeneral pvueMenuGeneral){
		
		vueMenugeneral=pvueMenuGeneral;
		
	}
	
	public void setModelHistorique(ModelHistorique pModelHistorique){
		
		modelHistorique = pModelHistorique;
	}
}

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.ScrollPane;
import java.awt.TextArea;
import java.io.IOException;
import javax.swing.JSpinner;
import java.awt.Scrollbar;


public class VueCours extends JFrame {

	public JPanel contentPane;
	public JLabel labelMenuGeneral;
	public JLabel labelInfo;
	public JLabel labelNom;
	public JLabel labelPrenom;
	public String nom;
	public String prenom;
	public VueLogin vuelogin;
	
	public VueMenuGeneral vueMenugeneral;
	public ModelHistorique modelHistorique;
	public VueDebutCours vueDebutCours;

	
	
	public VueCours() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 539, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 11, 531, 342);
		contentPane.add(panel);
		panel.setLayout(null);
		
		labelMenuGeneral = new JLabel("Cours donn\u00E9 en ce moment");
		labelMenuGeneral.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelMenuGeneral.setHorizontalAlignment(SwingConstants.CENTER);
		labelMenuGeneral.setBounds(23, 69, 267, 43);
		panel.add(labelMenuGeneral);
		
		labelInfo = new JLabel("Connect\u00E9 en tant que :");
		labelInfo.setBounds(55, 11, 130, 25);
		panel.add(labelInfo);
		
		labelNom = new JLabel("");
		labelNom.setBounds(195, 11, 110, 25);
		panel.add(labelNom);
		
		labelPrenom = new JLabel("");
		labelPrenom.setBounds(315, 11, 121, 25);
		panel.add(labelPrenom);
		
		JButton btnNewButton = new JButton("Retour Menu");
		btnNewButton.setBackground(new Color(60, 179, 113));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				setVisible(false);
				vueMenugeneral.setVisible(true);
				
			}
		});
		btnNewButton.setBounds(361, 68, 110, 44);
		panel.add(btnNewButton);
		
		JLabel labelDiscipline = new JLabel("Discipline :");
		labelDiscipline.setFont(new Font("Tahoma", Font.PLAIN, 13));
		labelDiscipline.setHorizontalAlignment(SwingConstants.LEFT);
		labelDiscipline.setBounds(55, 123, 84, 25);
		panel.add(labelDiscipline);
		
		JLabel labelDate = new JLabel("Date :");
		labelDate.setFont(new Font("Tahoma", Font.PLAIN, 13));
		labelDate.setHorizontalAlignment(SwingConstants.LEFT);
		labelDate.setBounds(55, 159, 84, 25);
		panel.add(labelDate);
		
		JLabel labelInfoDiscipline = new JLabel("");
		labelInfoDiscipline.setFont(new Font("Tahoma", Font.PLAIN, 13));
		labelInfoDiscipline.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoDiscipline.setBounds(164, 123, 141, 25);
		panel.add(labelInfoDiscipline);
		
		JLabel labelInfoDate = new JLabel("");
		labelInfoDate.setFont(new Font("Tahoma", Font.PLAIN, 13));
		labelInfoDate.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoDate.setBounds(164, 159, 141, 25);
		panel.add(labelInfoDate);
		
		JButton buttonNouveauCours = new JButton("Nouveau Cours");
		buttonNouveauCours.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				vueDebutCours.setVisible(true);
				setVisible(false);
			}
		});
		buttonNouveauCours.setBackground(new Color(64, 224, 208));
		buttonNouveauCours.setBounds(52, 272, 133, 44);
		panel.add(buttonNouveauCours);
		
		JLabel labelHeureDeDbut = new JLabel("Heure de d\u00E9but");
		labelHeureDeDbut.setFont(new Font("Tahoma", Font.PLAIN, 11));
		labelHeureDeDbut.setHorizontalAlignment(SwingConstants.LEFT);
		labelHeureDeDbut.setBounds(55, 195, 84, 25);
		panel.add(labelHeureDeDbut);
		
		JLabel labelInfoHeureDebut = new JLabel("");
		labelInfoHeureDebut.setFont(new Font("Tahoma", Font.PLAIN, 13));
		labelInfoHeureDebut.setHorizontalAlignment(SwingConstants.CENTER);
		labelInfoHeureDebut.setBounds(164, 195, 141, 25);
		panel.add(labelInfoHeureDebut);
		
		JButton btnArreter = new JButton("Arreter");
		btnArreter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnArreter.setBackground(new Color(178, 34, 34));
		btnArreter.setBounds(195, 272, 130, 44);
		panel.add(btnArreter);
		
		
	}
	
	
	

	
	public void setVueMenuGeneral(VueMenuGeneral pvueMenuGeneral){
		
		vueMenugeneral=pvueMenuGeneral;
		
	}
	
	public void setVueDebutCours(VueDebutCours pvueDebutCours){
		
		
		vueDebutCours = pvueDebutCours;
	}
	
}

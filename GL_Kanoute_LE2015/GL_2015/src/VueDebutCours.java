import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.util.Calendar;

import javax.swing.JComboBox;


public class VueDebutCours extends JFrame {

	public JPanel contentPane;
	public JLabel labelCours;
	public JLabel labelInfo;
	public JLabel labelNom;
	public JLabel labelPrenom;
	public String nom;
	public String prenom;
	public VueCours vueCours;
	public ModelCours modelCours;
	

	public VueDebutCours() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 539, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 11, 531, 342);
		contentPane.add(panel);
		panel.setLayout(null);
		
		labelCours = new JLabel("Choisissez votre discipline");
		labelCours.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelCours.setHorizontalAlignment(SwingConstants.CENTER);
		labelCours.setBounds(128, 61, 267, 35);
		panel.add(labelCours);
		
		labelInfo = new JLabel("Connect\u00E9 en tant que :");
		labelInfo.setBounds(55, 11, 130, 25);
		panel.add(labelInfo);
		
		labelNom = new JLabel("");
		labelNom.setBounds(195, 11, 110, 25);
		panel.add(labelNom);
		
		labelPrenom = new JLabel("");
		labelPrenom.setBounds(315, 11, 121, 25);
		panel.add(labelPrenom);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		comboBox.setBounds(55, 164, 185, 35);
		comboBox.addItem("Aerobic");
		comboBox.addItem("Boxe");
		comboBox.addItem("JuJitsu");
		comboBox.addItem("Karate");
		comboBox.addItem("MMA");
		comboBox.addItem("MuayThai");
		
		panel.add(comboBox);
		
		JButton btnNewButton = new JButton("Confirmer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String str= comboBox.getSelectedItem().toString();
				modelCours.ecrirCours(labelNom.getText(),labelPrenom.getText(),str);
				vueCours.setVisible(true);
				setVisible(false);
				
				
			}
		});
		btnNewButton.setBackground(new Color(60, 179, 113));
		btnNewButton.setBounds(55, 239, 130, 41);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Retour");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				vueCours.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton_1.setBackground(new Color(184, 134, 11));
		btnNewButton_1.setBounds(205, 239, 130, 41);
		panel.add(btnNewButton_1);
		
		
	}
	
	public void setModelCours(ModelCours pmodelCours){
		
		modelCours = pmodelCours;
		
	}
	
	public void setVueCours(VueCours pvueCours){
		
		vueCours=pvueCours;
		
	}
}

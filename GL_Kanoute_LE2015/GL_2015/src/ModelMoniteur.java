//Liste Moniteur pour tester le menu Login
public class ModelMoniteur {
	
	private String nomMoniteur;
	private String prenomMoniteur;
	
	public String[] tMoniteur = new String[10];
	
	public ModelMoniteur(){
		
		tMoniteur[0]="LE VAN-TAN";
		tMoniteur[1]="KANOUTE MAMADOU";
		tMoniteur[3]="KIRCHNER ANTONY";
		tMoniteur[4]="Coulombel Clothilde";
		tMoniteur[5]="Kazak Vadim";
		tMoniteur[6]="Badibanga Fabrice";
		tMoniteur[7]="Gnamba Ange";
		tMoniteur[8]="Peray Sebastien";
		tMoniteur[9]="Angelony Kyle";
		
		
	}
	
	public void setNomPrenom(String pNom, String pPrenom){
		
		nomMoniteur = pNom;
		prenomMoniteur = pPrenom;
		
	}
	
	
	
	public  boolean VerifMoniteur(String pNom, String pPrenom){
		
		String ConcatenationNomPrenom = pNom+" "+pPrenom;
		int i=0;
		boolean trouve = false;
		
		while (i<9 && trouve==false)
		{
			if(ConcatenationNomPrenom.equalsIgnoreCase(tMoniteur[i]))
			{
				
				trouve = true;
				
			}
		i++;
		}
		
		
		if (trouve==true){
			return true;
					}
			else{
				return false;
	
		}
	}
	
	
	public void ParcoursMoniteur(){
		
		for(int i=0;i<10;i++)
		{
			System.out.println(tMoniteur[i]);
		}
		
		
	}
	
	
}

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.SwingConstants;

import java.awt.Font;

//Fenetre de login, entrer Nom et Prenom en respectant bien la premi�re lettre en majuscule

public class VueLogin extends JFrame {

	public JPanel contentPane;
	public JTextField textFieldNom;
	public JTextField textFieldPrenom;
	public JButton btnSeLoguer;
	public VueMenuGeneral vueMenuGeneral;
	public ModelMoniteur modelMoniteur;
	public VueHistorique vueHistorique1;
	public VueCours vueCours1;
	public VueDebutCours vueDebutCours1;
	public String nom;
	public String prenom;


	public VueLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 541, 377);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		
		
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		textFieldNom = new JTextField();
		textFieldNom.setBounds(167, 113, 215, 35);
		panel.add(textFieldNom);
		textFieldNom.setColumns(10);
		
		JLabel lblGenevaMma = new JLabel("GENEVA MMA");
		lblGenevaMma.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblGenevaMma.setHorizontalAlignment(SwingConstants.CENTER);
		lblGenevaMma.setBounds(166, 47, 160, 44);
		panel.add(lblGenevaMma);
		
		JButton btnSeLoguer = new JButton("Se Loguer");
		
		
			
		btnSeLoguer.setFont(new Font("Tahoma", Font.PLAIN, 18));
			btnSeLoguer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				String nomMoniteur=textFieldNom.getText();
				String prenomMoniteur=textFieldPrenom.getText();
				
				
				boolean tested;
				
				tested = modelMoniteur.VerifMoniteur(nomMoniteur, prenomMoniteur);
				
				if(tested==true){
				
				
				vueMenuGeneral.labelNom.setText(nomMoniteur);
				vueMenuGeneral.labelPrenom.setText(prenomMoniteur);
				
				
				vueHistorique1.labelNom.setText(nomMoniteur);
				vueHistorique1.labelPrenom.setText(prenomMoniteur);
				vueHistorique1.setNomPrenom(nomMoniteur, prenomMoniteur);
				
				vueCours1.labelNom.setText(nomMoniteur);
				vueCours1.labelPrenom.setText(prenomMoniteur);
				
				vueDebutCours1.labelNom.setText(nomMoniteur);
				vueDebutCours1.labelPrenom.setText(prenomMoniteur);
				
				vueMenuGeneral.setVisible(true);
				setVisible(false);
				}
				
				
			}
		});
		
		
		
		
		btnSeLoguer.setBounds(186, 255, 122, 50);
		panel.add(btnSeLoguer);
		
		JLabel labelNom = new JLabel("NOM");
		labelNom.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelNom.setHorizontalAlignment(SwingConstants.CENTER);
		labelNom.setBounds(24, 110, 73, 35);
		panel.add(labelNom);
		
		textFieldPrenom = new JTextField();
		textFieldPrenom.setBounds(167, 180, 215, 35);
		panel.add(textFieldPrenom);
		textFieldPrenom.setColumns(10);
		
		JLabel labelPrenom = new JLabel("PRENOM");
		labelPrenom.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelPrenom.setHorizontalAlignment(SwingConstants.CENTER);
		labelPrenom.setBounds(24, 177, 100, 35);
		panel.add(labelPrenom);
	}
	
	public void setModelMoniteur(ModelMoniteur pModelMoniteur){
		modelMoniteur = pModelMoniteur;
		
		
	}
	
	public void setMenuGeneral(VueMenuGeneral pMenuGeneral)
	{
		vueMenuGeneral=pMenuGeneral;
	}
	
	public void setVueHistorique(VueHistorique pVueHistorique){
		
		
		vueHistorique1 = pVueHistorique;
	}
	
	public void setVueCours(VueCours pvueCours){
		
		vueCours1=pvueCours;
		
	}
	
	public void setVueDebutCours(VueDebutCours pvueDebutCours){
		
		vueDebutCours1 = pvueDebutCours;
	}
	
	public String getNom(){
		return nom;
	}
	
	public String getPrenom(){
		return prenom;
		
	}
}

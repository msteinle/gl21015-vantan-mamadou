import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

//Ici le menu g�n�ral permet d'orienter vers les diff�rentes fen�tres voulus et permet aussi de se d�connecter

public class VueMenuGeneral extends JFrame {

	public JPanel contentPane;
	public JLabel labelMenuGeneral;
	public JLabel labelInfo;
	public JLabel labelNom;
	public JLabel labelPrenom;
	public String nom;
	public String prenom;
	public VueLogin vuelogin;
	public VueHistorique vueHistorique;
	public VueCours vueCours;
	

	public VueMenuGeneral() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 539, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 11, 531, 342);
		contentPane.add(panel);
		panel.setLayout(null);
		
		labelMenuGeneral = new JLabel("Menu G\u00E9n\u00E9ral");
		labelMenuGeneral.setFont(new Font("Tahoma", Font.PLAIN, 18));
		labelMenuGeneral.setHorizontalAlignment(SwingConstants.CENTER);
		labelMenuGeneral.setBounds(182, 62, 140, 35);
		panel.add(labelMenuGeneral);
		
		labelInfo = new JLabel("Connect\u00E9 en tant que :");
		labelInfo.setBounds(55, 11, 130, 25);
		panel.add(labelInfo);
		
		labelNom = new JLabel("");
		labelNom.setBounds(195, 11, 110, 25);
		panel.add(labelNom);
		
		labelPrenom = new JLabel("");
		labelPrenom.setBounds(315, 11, 121, 25);
		panel.add(labelPrenom);
		
		JButton btnHistorique = new JButton("Consulter historique");
		btnHistorique.setBackground(new Color(218, 165, 32));
		btnHistorique.setFont(new Font("Arial", Font.BOLD, 12));
		
		
		
		btnHistorique.setBounds(78, 237, 160, 52);
		panel.add(btnHistorique);
		
		JButton btnServiceEnCours = new JButton("Service en cours");
		btnServiceEnCours.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false);
				vueCours.setVisible(true);
			}
		});
		btnServiceEnCours.setBackground(new Color(144, 238, 144));
		btnServiceEnCours.setFont(new Font("Arial", Font.BOLD, 12));
		btnServiceEnCours.setBounds(76, 155, 162, 52);
		panel.add(btnServiceEnCours);
		
		JButton btnDeconnection = new JButton("Se d\u00E9connecter");
		btnDeconnection.setFont(new Font("Arial", Font.BOLD, 12));
		btnDeconnection.setBackground(new Color(205, 92, 92));
		btnDeconnection.setBounds(289, 156, 147, 52);
		panel.add(btnDeconnection);
		
		
		//Evenement lorsqu'on clique sur le bouton DECONNEXTION
		btnDeconnection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false);
				vuelogin.setVisible(true);
				
				
				
			}
		});
		//Evenement lorsqu'on clique sur le bouton HISTORIQUE
				btnHistorique.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						
						setVisible(false);
						vueHistorique.modelHistorique.setNom(labelNom.getText());
						vueHistorique.modelHistorique.setPrenom(labelPrenom.getText());
						vueHistorique.textArea.setText("");
						String str;
						
						for (int i=0;i<300-1;i++){
							
							
							str = vueHistorique.modelHistorique.fonctionHistorique(i);
							if (str!="espace"){

								vueHistorique.textArea.setText(str+" \n"+vueHistorique.textArea.getText());
								
							}
						}
						
						
						
					
						vueHistorique.setVisible(true);
						
					}
				});
		
		
	}
	
	//Mutateur vueLogin
	public void setVueLogin(VueLogin pvuelogin){
		vuelogin=pvuelogin;
		
	}
	//Mutateur vueHistorique
	public void setVueHistorique(VueHistorique pvueHistorique){
		vueHistorique = pvueHistorique;
		
	}
	
	public void setVueCours(VueCours pvueCours){
		
		vueCours=pvueCours;
		
	}
	
}
